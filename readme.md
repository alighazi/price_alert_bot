commands
price - get the price for desired coin
p - same as /price
chart - chart for a coin at timeframe (5m, 15m, 1h, 4h, 1d, 1w, etc)
ch - same as /chart
top - See the current prices of the top coins + market cap
lower - get notified when price goes LOWER than specified number
higher - get notified when price goes HIGHER than specified number
alerts - get current alert
clear - clear current alert
help - get help about commands